"use strict";


Template.home.onCreated(function () {
  Meteor.subscribe("posts");
  Meteor.subscribe("users");
  Meteor.subscribe("emojis");
});

Template.home.helpers({
  friends: function () {
    if (Meteor.user().profile) {
      var friends = Meteor.user().profile.friends;
      var friendsList = [];

      for (var i = 0; i < friends.length; i++) {
        friendsList.push(Meteor.users.findOne(friends[i]));
      }

      return friendsList;
    }
  },

  postList: function () {
    return Posts.find({}).fetch();
  },

  parseMessage: function (message) {
    return Emojis.parse(message);
  }

});

Template.home.events({
  "submit .message-form": function (event) {
    event.preventDefault();
    var name = Meteor.user().username;
    var message = event.target.chat.value;
    var date = moment().format("MMMM Do, h:mm:ss a");

    Posts.insert({
      author: name,
      chat: message,
      createdAt: date,
      private: false
    });

    event.target.chat.value = "";
  }

  //  "click .message-friend": function () {
  //    var post = Posts.insert({
  //      players: [
  //        {
  //          player: Meteor.userId(),
  //
  //        },
  //        {
  //          player: this._id,
  //
  //        }
  //      ],
  //      private: true;
  //   });
  //
  //    Router.go("message", {
  //     name: 
  //    });
  //  },
});